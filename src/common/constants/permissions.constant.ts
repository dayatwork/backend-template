export const PERMISSIONS = {
  ROLE: {
    MANAGE: 'manage:role',
    CREATE: 'create:role',
    READ: 'read:role',
    UPDATE: 'update:role',
    DELETE: 'delete:role',
  },
  USER: {
    MANAGE: 'manage:user',
    CREATE: 'create:user',
    READ: 'read:user',
    UPDATE: 'update:user',
    DELETE: 'delete:user',
    SET_ROLE: 'set-role:user',
  },
};
