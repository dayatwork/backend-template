import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const [req] = context.getArgs();

    const userRoles = req.user.roles || [];
    const userPermissions = req?.user?.permissions || [];

    const isSuperAdmin = userRoles.includes('super-admin');

    const requiredPermissions =
      this.reflector.get('permissions', context.getHandler()) || [];

    const requiredPermissionsResources = requiredPermissions.map(
      (permission) => `manage:${permission.split(':')[1]}`,
    );

    const hasAccessToManageResource = userPermissions.some((permission) =>
      requiredPermissionsResources.includes(permission),
    );

    const hasAllRequiredPermissions = requiredPermissions.every((permission) =>
      userPermissions.includes(permission),
    );

    if (
      requiredPermissions.length === 0 ||
      isSuperAdmin ||
      hasAccessToManageResource ||
      hasAllRequiredPermissions
    ) {
      return true;
    }

    throw new ForbiddenException('Insufficient Permissions');
  }
}
