import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  FilterOperator,
  paginate,
  Paginated,
  PaginateQuery,
} from 'nestjs-paginate';
import { PERMISSIONS } from 'src/common/constants/permissions.constant';
import { In, Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
  ) {}

  create(createRoleDto: CreateRoleDto) {
    const role = this.rolesRepository.create(createRoleDto);
    return this.rolesRepository.save(role);
  }

  findAll(query: PaginateQuery): Promise<Paginated<Role>> {
    return paginate(query, this.rolesRepository, {
      sortableColumns: ['id', 'name', 'display_name'],
      searchableColumns: ['name'],
      defaultSortBy: [['id', 'DESC']],
      filterableColumns: {
        name: [FilterOperator.EQ],
        display_name: [FilterOperator.EQ],
      },
      relations: ['users'],
      select: ['id', 'name', 'display_name', 'users.id', 'users.name'],
    });
  }

  async findById(id: number) {
    const role = await this.rolesRepository.findOne({
      where: { id },
    });
    if (!role) {
      throw new NotFoundException('Role not found');
    }
    return role;
  }

  findByIds(ids: number[]) {
    return this.rolesRepository.findBy({ id: In(ids) });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    const role = await this.rolesRepository.preload({ id, ...updateRoleDto });
    if (!role) {
      throw new NotFoundException('Role not found');
    }
    return this.rolesRepository.save(role);
  }

  async delete(id: number) {
    const role = await this.findById(id);
    return this.rolesRepository.remove(role);
  }

  findAvailablePermissions() {
    return Object.entries(PERMISSIONS).map(([key, value]) => ({
      resource: key,
      permissions: Object.entries(value).map(([, value]) => value),
    }));
  }
}
