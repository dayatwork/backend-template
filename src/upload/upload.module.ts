import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { S3Module } from 'src/s3/s3.module';
import { UploadController } from './upload.controller';
import { UploadService } from './upload.service';

@Module({
  imports: [S3Module, ConfigModule],
  controllers: [UploadController],
  providers: [UploadService],
})
export class UploadModule {}
