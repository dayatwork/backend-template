import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectS3, S3 } from 'src/s3';

@Injectable()
export class UploadService {
  constructor(
    @InjectS3() private readonly s3: S3,
    private configService: ConfigService,
  ) {}

  async upload(file: Express.Multer.File) {
    return this.s3
      .upload({
        Bucket: this.configService.get('S3_BUCKET'),
        Key: file.originalname,
        Body: file.buffer,
        ACL: 'public-read',
      })
      .promise();
  }
}
