export type SendWhatsappResponse = {
  status: number;
  error: {
    code: string;
    message: string;
    field: string;
  };
  data: {
    success: boolean;
    message: string;
    reason: string;
    id: string;
  };
};
