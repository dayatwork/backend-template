import { IsEnum, IsPhoneNumber, IsString } from 'class-validator';

export class SendWhatsappMessageDto {
  @IsPhoneNumber()
  phone: string;

  @IsEnum(['otp', 'text', 'image'])
  messageType: string;

  @IsString()
  body: string;
}
