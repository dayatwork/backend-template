import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map } from 'rxjs';
import { SendWhatsappMessageDto } from './dto/send-whatsapp-message.dto';
import appConfig from 'src/config/app.config';
import { SendWhatsappResponse } from './types/send-whatsapp-response.type';

@Injectable()
export class WhatsappService {
  constructor(private readonly httpService: HttpService) {}

  async sendWhatsapp(body: SendWhatsappMessageDto) {
    const { sendTalkApiKey, sendTalkApiUrl } = appConfig();

    const URL = sendTalkApiUrl + '/send_whatsapp';

    const data: SendWhatsappMessageDto = {
      ...body,
      phone: body.phone.replace('+', ''),
    };

    return this.httpService
      .post<SendWhatsappResponse>(URL, data, {
        headers: {
          'API-Key': sendTalkApiKey,
          'Content-Type': 'application/json',
        },
      })
      .pipe(map((response) => response.data));
  }
}
