import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { PERMISSIONS } from 'src/common/constants/permissions.constant';
import { Permissions } from 'src/common/decorators/permissions.decorator';
import { PermissionsGuard } from 'src/common/guards/permissions.guard';
import { SetUserRolesDto } from './dto/set-user-roles.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@UseGuards(PermissionsGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Permissions(PERMISSIONS.USER.READ)
  @Get()
  findAll(@Paginate() query: PaginateQuery): Promise<Paginated<User>> {
    return this.usersService.findAll(query);
  }

  @Permissions(PERMISSIONS.USER.READ)
  @Get(':id')
  findById(@Param('id') id: string) {
    return this.usersService.findById(id);
  }

  @Permissions(PERMISSIONS.USER.SET_ROLE)
  @Post('roles')
  setUserRoles(@Body() setUserRolesDto: SetUserRolesDto) {
    const { userId, rolesIds } = setUserRolesDto;
    return this.usersService.setUserRoles(userId, rolesIds);
  }

  @Post('send-email')
  sendEmail(@Body('email') email: string) {
    return this.usersService.sendTestEmail(email);
  }

  @Post('send-whatsapp')
  sendWhatsapp(@Body('phone') phone: string) {
    return this.usersService.sendTestWhatsapp(phone);
  }
}
