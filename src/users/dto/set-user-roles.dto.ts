import { IsNumber, IsString } from 'class-validator';

export class SetUserRolesDto {
  @IsString()
  userId: string;

  @IsNumber({}, { each: true })
  rolesIds: number[];
}
