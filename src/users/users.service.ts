import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import {
  FilterOperator,
  paginate,
  Paginated,
  PaginateQuery,
} from 'nestjs-paginate';
import { ServerClient } from 'postmark';
import { RolesService } from 'src/roles/roles.service';
import { POSTMARK } from 'src/postmark/postmark.constant';
import { WhatsappService } from 'src/whatsapp/whatsapp.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly rolesServices: RolesService,
    private readonly whatsappService: WhatsappService,
    @Inject(POSTMARK) private readonly postmarkClient: ServerClient,
  ) {}

  hashPassword(password: string) {
    return bcrypt.hash(password, 10);
  }

  comparePassword(password: string, hashedPassword: string) {
    return bcrypt.compare(password, hashedPassword);
  }

  async create(createUserDto: CreateUserDto) {
    const { password, ...userData } = createUserDto;
    const hashedPassword = await this.hashPassword(password);
    const user = this.userRepository.create({
      ...userData,
      password: hashedPassword,
    });
    return this.userRepository.save(user);
  }

  async findAll(query: PaginateQuery): Promise<Paginated<User>> {
    return paginate(query, this.userRepository, {
      sortableColumns: ['id', 'name', 'email', 'phone'],
      searchableColumns: ['name', 'email', 'phone'],
      defaultSortBy: [['id', 'DESC']],
      filterableColumns: {
        name: [FilterOperator.EQ],
        email: [FilterOperator.EQ],
        phone: [FilterOperator.EQ],
        'roles.name': [FilterOperator.EQ],
      },
      relations: ['roles'],
      select: [
        'id',
        'name',
        'email',
        'phone',
        'roles.id',
        'roles.name',
        'roles.display_name',
      ],
    });
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      where: { email },
      relations: {
        roles: true,
      },
    });
  }

  async findByIdIncludePassword(id: string) {
    return this.userRepository.findOne({
      where: { id },
      relations: {
        roles: true,
      },
    });
  }

  async findById(id: string) {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: {
        roles: true,
      },
      select: {
        id: true,
        name: true,
        email: true,
        phone: true,
        is_active: true,
        roles: {
          id: true,
          name: true,
          permissions: true,
        },
      },
    });

    const rolePermissions = user.roles.map((role) => role.permissions);

    const formattedUser = {
      ...user,
      roles: user.roles.map((role) => ({
        id: role.id,
        name: role.name,
      })),
      permissions: [...new Set([].concat(...rolePermissions))],
    };

    return formattedUser;
  }

  async setUserRoles(userId: string, rolesIds: number[]) {
    const user = await this.findById(userId);
    const roles = await this.rolesServices.findByIds(rolesIds);
    user.roles = roles;
    return this.userRepository.save(user);
  }

  async sendTestEmail(email: string) {
    await this.postmarkClient.sendEmail({
      From: 'gmail@dayat.dev',
      // To: email,
      To: 'gmail@dayat.dev',
      Subject: 'Test',
      TextBody: 'Hello from Postmark!',
    });

    return {
      message: `Email sent to ${email}`,
    };
  }

  async sendTestWhatsapp(phone: string) {
    return this.whatsappService.sendWhatsapp({
      phone,
      messageType: 'text',
      body: 'Hello from Whatsapp!',
    });
  }

  async changePassword({
    userId,
    current_password,
    new_password,
  }: {
    userId: string;
    current_password: string;
    new_password: string;
  }) {
    const user = await this.findByIdIncludePassword(userId);

    if (!(await this.comparePassword(current_password, user.password))) {
      throw new BadRequestException('Current password is not valid');
    }

    const hashedPassword = await this.hashPassword(new_password);
    user.password = hashedPassword;
    const savedUser = await this.userRepository.save(user);
    savedUser.password = undefined;
    return savedUser;
  }
}
