import { DynamicModule, Module, Provider } from '@nestjs/common';
import * as postmark from 'postmark';
import { POSTMARK } from './postmark.constant';

@Module({})
export class PostmarkModule {
  static forRoot(
    serverToken: string,
    config?: postmark.Models.ClientOptions.Configuration,
  ): DynamicModule {
    const client = new postmark.ServerClient(serverToken, config);

    const postmarkProvider: Provider = {
      provide: POSTMARK,
      useValue: client,
    };

    return {
      module: PostmarkModule,
      providers: [postmarkProvider],
      exports: [postmarkProvider],
      global: true,
    };
  }
}
