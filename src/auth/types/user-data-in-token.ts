export type UserDataInToken = {
  id: string;
  email: string;
  phone: string;
};

export type UserDataInRefreshToken = {
  id: string;
  refreshToken: string;
};
