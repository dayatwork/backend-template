import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';
import { Public } from 'src/common/decorators/public.decorator';
import { AuthService } from './auth.service';
import { User } from './decorators/user.decorator';
import { ChangePasswordDto } from './dto/change-password.dto';
import { SignUpDto } from './dto/signup.dto';
import { JwtRefreshGuard } from './guards/jwt-refresh.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import {
  UserDataInRefreshToken,
  UserDataInToken,
} from './types/user-data-in-token';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post('signup')
  signup(
    @Body() signupDto: SignUpDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.authService.signup(signupDto, res);
  }

  @Public()
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  @Post('login')
  login(
    @User() user: UserDataInToken,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.authService.login(user, res);
  }

  @Get('me')
  getCurrentUser(@User() user: UserDataInToken) {
    return this.authService.getCurrentUser(user);
  }

  @Post('change-password')
  async changePassword(
    @User() user: UserDataInToken,
    @Body() changePasswordDto: ChangePasswordDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.authService.changePassword(user, changePasswordDto, res);
  }

  @Public()
  @UseGuards(JwtRefreshGuard)
  @HttpCode(HttpStatus.OK)
  @Post('refresh')
  refreshToken(
    @User() user: UserDataInRefreshToken,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.authService.refreshToken(user, res);
  }

  @HttpCode(HttpStatus.OK)
  @Post('logout')
  logout(@Res({ passthrough: true }) res: Response) {
    return this.authService.logout(res);
  }

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('clear')
  clear(@Res({ passthrough: true }) res: Response) {
    return this.authService.clear(res);
  }
}
