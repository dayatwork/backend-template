import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JsonWebTokenError } from 'jsonwebtoken';

@Injectable()
export class JwtRefreshGuard extends AuthGuard('jwt-refresh') {
  handleRequest<TUser = any>(
    err: any,
    user: any,
    info: any,
    context: ExecutionContext,
    status?: any,
  ): TUser {
    if (info?.message === 'No auth token') {
      throw new UnauthorizedException('NO_REFRESH_TOKEN');
    }
    if (info instanceof JsonWebTokenError) {
      throw new UnauthorizedException('INVALID_REFRESH_TOKEN');
    }
    return super.handleRequest(err, user, info, context, status);
  }
}
