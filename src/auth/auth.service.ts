import { InjectRedis } from '@liaoliaots/nestjs-redis';
import { ForbiddenException, Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Response } from 'express';
import Redis from 'ioredis';
import { UsersService } from 'src/users/users.service';
import authConfig from './config/auth.config';
import { ChangePasswordDto } from './dto/change-password.dto';
import { SignUpDto } from './dto/signup.dto';
import {
  UserDataInRefreshToken,
  UserDataInToken,
} from './types/user-data-in-token';

type Token = {
  accessToken: string;
  refreshToken: string;
};

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    @Inject(authConfig.KEY)
    private readonly config: ConfigType<typeof authConfig>,
    @InjectRedis() private readonly redis: Redis,
  ) {}

  comparePassword(password: string, hashedPassword: string) {
    return bcrypt.compare(password, hashedPassword);
  }

  async signTokens(payload: any, res: Response): Promise<Token> {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          sub: payload.id,
          email: payload.email,
          phone: payload.phone,
          rolesIds: payload.roles?.map((role) => role.id) || [],
        },
        {
          expiresIn: this.config.accessTokenExpiresIn,
          secret: this.config.accessTokenSecret,
        },
      ),
      this.jwtService.signAsync(
        {
          sub: payload.id,
        },
        {
          expiresIn: this.config.refreshTokenExpiresIn,
          secret: this.config.refreshTokenSecret,
        },
      ),
    ]);

    res.cookie('access_token', accessToken, {
      httpOnly: true,
      maxAge: this.config.accessTokenExpiresIn * 1000,
    });
    res.cookie('refresh_token', refreshToken, {
      httpOnly: true,
      maxAge: this.config.refreshTokenExpiresIn * 1000,
    });

    await this.redis.set(
      `refresh_token:${refreshToken}`,
      '1',
      'EX',
      this.config.refreshTokenExpiresIn,
    );

    return {
      accessToken,
      refreshToken,
    };
  }

  async signup(signupDto: SignUpDto, res: Response) {
    const user = await this.usersService.create(signupDto);

    const [token, userData] = await Promise.all([
      this.signTokens(user, res),
      this.getCurrentUser(user),
    ]);
    return {
      ...token,
      user: userData,
    };
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findByEmail(email);

    if (user && (await this.comparePassword(password, user.password))) {
      return user;
    }
    return null;
  }

  async login(user: UserDataInToken, res: Response) {
    const [token, userData] = await Promise.all([
      this.signTokens(user, res),
      this.getCurrentUser(user),
    ]);
    return {
      ...token,
      user: userData,
    };
  }

  async getCurrentUser(user: UserDataInToken) {
    return this.usersService.findById(user.id);
  }

  async refreshToken(userData: UserDataInRefreshToken, res: Response) {
    const storedRefreshToken = await this.redis.get(
      `refresh_token:${userData.refreshToken}`,
    );

    if (!storedRefreshToken) {
      throw new ForbiddenException('Refresh token is not valid');
    }

    const user = await this.usersService.findById(userData.id);

    return this.signTokens(user, res);
  }

  async logout(res: Response) {
    res.clearCookie('access_token');
    res.clearCookie('refresh_token');
    return { message: 'Logout successful' };
  }

  async clear(res: Response) {
    res.clearCookie('access_token');
    res.clearCookie('refresh_token');
    return { message: 'Logout successful' };
  }

  async changePassword(
    user: UserDataInToken,
    changePasswordDto: ChangePasswordDto,
    res: Response,
  ) {
    const { current_password, new_password } = changePasswordDto;

    const savedUser = await this.usersService.changePassword({
      userId: user.id,
      current_password,
      new_password,
    });

    const token = await this.signTokens(savedUser, res);

    return {
      ...token,
      user: savedUser,
    };
  }
}
