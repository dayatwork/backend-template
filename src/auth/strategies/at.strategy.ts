import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import appConfig from 'src/config/app.config';
import { RolesService } from 'src/roles/roles.service';

const cookieExtractor = (req: Request) => {
  let token = null;
  if (req && req.cookies) {
    token = req.cookies.access_token;
  }
  return token;
};

@Injectable()
export class AtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly rolesServices: RolesService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        cookieExtractor,
        ExtractJwt.fromAuthHeaderAsBearerToken(),
      ]),
      ignoreExpiration: false,
      secretOrKey: appConfig().accessTokenSecret,
    });
  }

  async validate(payload: any) {
    const roles = await this.rolesServices.findByIds(payload.rolesIds);
    const rolesPermissions = roles.map((role) => role.permissions);

    return {
      id: payload.sub,
      email: payload.email,
      phone: payload.phone,
      roles: roles.map((role) => role.name),
      permissions: [...new Set([].concat(...rolesPermissions))],
    };
  }
}
